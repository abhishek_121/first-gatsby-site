import React from 'react'
import Layout from '../components/Layout'

export default function About() {
  return (
    <Layout>
      <div>
        <h1>About Page</h1>
        <p>This section is about the gatsby site.</p><br/>
        <p>Gatsby is an open-source static site generator built on top of Node.js using React and GraphQL.
           It provides over 2500 plugins to create static sites based on sources as Markdown documents,
            MDX (Markdown with JSX), images, and numerous Content Management Systems such as WordPress, 
            Drupal and more. Since version 4 Gatsby also supports Server-Side Rendering
             and Deferred Static Generation for rendering dynamic websites on a Node.js server.
              Gatsby is developed by Gatsby, Inc. which also offers a cloud service, Gatsby Cloud,
               for hosting Gatsby websites.</p>
      </div>
    </Layout>
    
  )
}
