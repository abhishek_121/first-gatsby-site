---
title: The Cafe Coffee Day
stack: HTML & CSS
slug: the-cafe-coffee-day
date: 2022-07-03T00:00:00+00:00
---


**Café Coffee Day (CCD)**, is an *Indian multinational chain of coffeehouses* headquartered in Bengaluru, Karnataka. It is a subsidiary of Coffee Day Enterprises Limited. Internationally, CCDs are present in Austria, Czech Republic, Malaysia, Nepal and Egypt.

### History

Café Coffee Day Global Limited Company is a Chikkamagaluru-based business which grows coffee in its own estates of 20,000 acres. It is the largest producer of arabica beans in Asia, exporting to various countries including U.S., Europe, and Japan.

V. G. Siddhartha started the café chain in 1996 when he incorporated Coffee Day Global, which is the parent of the Coffee Day chain. The first CCD outlet was set up on July 11, 1996, at Brigade Road, Bangalore, Karnataka. It rapidly expanded to other cities in India, with more than 1,000 cafés open across the nation by 2011.